export const categoryIds = {
  index: 0,
  sport: 2,
  technologies: 1,
  politics: 4,
  fashion: 3,
};

export const categoryNames = {
  index: "Главная",
  fashion: "Мода",
  technologies: "Технологии",
  politics: "Политика",
  sport: "Спорт",
};
